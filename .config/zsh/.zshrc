zstyle ':z4h:'                  auto-update             no
zstyle ':z4h:'                  auto-update-days        28
zstyle ':z4h:'                  start-tmux              no
zstyle ':z4h:'                  term-shell-integration  yes
zstyle ':z4h:autosuggestions'   forward-char            partial-accept
zstyle ':z4h:autosuggestions'   end-of-line             partial-accept
zstyle ':z4h:bindkey'           keyboard                pc
zstyle ':z4h:direnv'            enable                  no
zstyle ':z4h:direnv:success'    notify                  no
zstyle ':z4h:fzf-complete'      recurse-dirs            no
zstyle ':z4h:fzf-complete'      fzf-bindings            tab:repeat
zstyle ':z4h:*'                 fzf-flags               --color=hl:12,hl+:12
zstyle ':z4h:ssh:*'             enable                  no
zstyle ':z4h:ssh:*'             send-extra-files        '~/.nanorc'
# TODO tweak (fzf) completion behaviors
# - history shadow: forward: word or all
# - history search: make list unique
# - command tab: fix default sort shortest (git comm -> commit-tree)

z4h install reegnz/jq-zsh-plugin || return
z4h install wfxr/forgit || return

z4h init || return

z4h bindkey z4h-cd-back         Alt+Left
z4h bindkey z4h-cd-forward      Alt+Right
z4h bindkey z4h-kill-zword      Ctrl+X
z4h bindkey yank                Ctrl+V
z4h bindkey undo                Ctrl+Z
z4h bindkey redo                Ctrl+Y
z4h bindkey z4h-exit            Ctrl+D

z4h source /opt/asdf-vm/asdf.sh
z4h source ~/.config/zsh/autovenv.zsh
z4h source ~/.config/zsh/env.zsh
z4h source ~/.config/zsh/ssh-agent.zsh
z4h source ~/.google-cloud-sdk/path.zsh.inc
z4h source ~/.google-cloud-sdk/completion.zsh.inc
z4h source reegnz/jq-zsh-plugin/jq.plugin.zsh
z4h source wfxr/forgit/forgit.plugin.zsh

eval "$(direnv hook zsh)"
eval "$(zoxide init zsh)"
test -z "$z4h_win_home" || hash -d w=$z4h_win_home
path=(~/.local/bin $path)
fpath=(~/.local/zsh-completion $fpath)

export BAT_THEME='Visual Studio Dark+'
export DOCKER_BUILDKIT=1
export EDITOR=micro
export EGET_BIN=~/.local/bin
export GHORG_GITHUB_TOKEN=$GITHUB_TOKEN
export GHORG_GITLAB_TOKEN=$GITLAB_TOKEN
export LESS=-FiMRXx4
export FORGIT_FZF_DEFAULT_OPTS='--height 100%'
export FZF_DEFAULT_OPTS='--layout=reverse'
export PAGER="less $LESS"
export PIPENV_HIDE_EMOJIS=1
export POETRY_INSTALLER_MAX_WORKERS=10
export POETRY_VIRTUALENVS_IN_PROJECT=true
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

alias cat=bat
alias dotfiles='git --git-dir="$HOME/.config/dotfiles" --work-tree="$HOME"'
alias du=gdu
alias grep='grep -E --color=auto --exclude-dir={.git}'
alias kubectl=kubecolor
alias ls='lsd -ahl --group-dirs=first --date="+%F %T" --icon=never'
alias ps=procs
alias sed='sed -E'
alias top=btop
alias tree='tree -a --dirsfirst --gitignore -I .git'
alias zshrc='micro ~/.config/zsh/.zshrc && exec zsh'

c256() { for i in {0..255}; do print -Pn "%K{$i}  %k%F{$i}${(l:3::0:)i}%f " ${${(M)$((i%6)):#3}:+$'\n'}; done }
f() { fff "$@"; cd "$(cat "${XDG_CACHE_HOME:=$HOME/.cache}/fff/.fff_d")"; }
hgrep() { command hgrep -c2 -C5 --term-width "$COLUMNS" "$@" | less; test "${pipestatus[1]}" = 0; }
help() { tldr "$1" 2>/dev/null || man "$1"; }
man() {
    if type "$1" | grep -q builtin
    then run-help "$1"
    else command man "$1" | col -bx | bat -pl man
    fi
}
md() { [[ $# = 1 ]] && mkdir -p -- "$1" && cd -- "$1"; }
tpl() { cat "$1" | env "${@:2}" envsubst >"${1#.*}"; }
extract() {
    test -f "$1" || { echo >&2 "'$1' is not a file"; return 1; }
    case "$1" in
        *.7z)               7z x            "$1";;
        *.bz2)              bunzip2         "$1";;
        *.gz)               gunzip          "$1";;
        *.rar)              rar x           "$1";;
        *.tar.bz2|*.tbz2)   tar xjf         "$1";;
        *.tar.gz |*.tgz)    tar xzf         "$1";;
        *.tar.xz |*.txz)    tar xJf         "$1";;
        *.tar.zst)          tar --zstd xf   "$1";;
        *.tar)              tar xf          "$1";;
        *.Z)                uncompress      "$1";;
        *.zip)              unzip           "$1";;
        *.zst)              unzstd          "$1";;
        *)  echo >&2 "cannot extract '$1' (unhandled extension)"
            return 1;;
    esac
}

autoload -Uz zmv
complete -C "$(command -v aws_completer)" aws

compdef _directories    md
compdef _docker         d
compdef _docker-compose dc  # TODO
compdef _git            g   # TODO
compdef _kubectl        k

# https://zsh.sourceforge.io/Doc/Release/Options.html
setopt auto_menu
setopt glob_dots
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt hist_save_no_dups
