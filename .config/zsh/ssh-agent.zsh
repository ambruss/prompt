#!/usr/bin/env zsh
ssh_agent_log="$XDG_RUNTIME_DIR/ssh-agent.log"
ssh_agent_pid_file="$XDG_RUNTIME_DIR/ssh-agent.pid"
SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.sock"
SSH_AGENT_PID="${SSH_AGENT_PID:-$(cat "$ssh_agent_pid_file" 2>/dev/null)}"
if ! kill -0 "$SSH_AGENT_PID" &>/dev/null; then
    rm -rf "$SSH_AUTH_SOCK" &>/dev/null
    eval "$(ssh-agent -sa"$SSH_AUTH_SOCK" &>"$ssh_agent_log")"
    echo "$SSH_AGENT_PID" >"$ssh_agent_pid_file"
    find ~/.ssh -type f -name "id*" | grep -v pub | xargs ssh-add &>>"$ssh_agent_log"
fi
if ! kill -0 "$SSH_AGENT_PID" &>/dev/null; then
    echo "$0: error: could not start ssh agent:\n$(cat $ssh_agent_log)"
    exit 1
fi
export SSH_AGENT_PID
export SSH_AUTH_SOCK
