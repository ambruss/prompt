c = get_config()
c.InteractiveShellApp.exec_lines = [
    "load_ext autoreload",
    "autoreload 2",
]
c.TerminalIPythonApp.display_banner = False
c.TerminalInteractiveShell.auto_match = True
c.TerminalInteractiveShell.confirm_exit = False
