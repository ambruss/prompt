# ambruss/dotfiles

Development environment bootstrapper for installing/updating
packages and user dotfiles (like `.zshrc`) on Arch Linux.

## Install

```bash
bash -c "$(curl -fLSs https://gitlab.com/ambruss/dotfiles/-/raw/main/install.sh)"
```

## License

MIT
