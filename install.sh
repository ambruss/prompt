#!/usr/bin/env bash
set -Eeuo pipefail
PS4='+ ${BASH_SOURCE:-dotfiles/install.sh}:$LINENO ${FUNCNAME[0]:+${FUNCNAME[0]}()}: '
test -n "${TRACE:-}" && set -x || trap "err ${PS4:2} \$BASH_COMMAND exited with \$?" ERR

main() {
    log "running dotfiles/install.sh"
    test "$(id -un)" != root || die "cannot run as root"
    sudo pacman -h &>/dev/null || die "cannot run 'sudo pacman'"
    trap "rm -rf ${tmp:=$(mktemp -d)}" EXIT

    log "upgrading existing packages"
    sudo pacman -Syu --noconfirm

    log "installing pacman base packages"
    sudo pacman -S --needed --noconfirm base-devel git go-yq

    if ! has_cmd yay; then
        log "installing yay"
        git clone https://aur.archlinux.org/yay-bin.git "$tmp/yay-bin"
        (cd "$tmp/yay-bin" && makepkg -si --needed --noconfirm)
        yay -Y --gendb
    fi

    log "installing yay packages"
    apply add_yay_pkg "${yay_pkgs[@]}"
    apply add_gui_pkg "${gui_pkgs[@]}"
    has_cmd asdf || . /opt/asdf-vm/asdf.sh
    ! has_yay_pkg displaylink || svc_enable displaylink

    log "installing eget binaries"
    export EGET_BIN=$local_bin
    export PATH=$local_bin:$PATH
    mkdir -p "$local_bin" "$local_comp"
    add_eget_bin casey/just
    add_eget_bin GoogleContainerTools/skaffold
    add_eget_bin kurtbuilds/checkexec
    add_eget_bin watchexec/watchexec -amusl.tar -a"^xz."
    teleport_version=10.3.15
    eget https://cdn.teleport.dev/teleport-v${teleport_version}-linux-amd64-bin.tar.gz -ftsh

    if ! has_cmd aws; then
        log "installing awscli"
        curl -fLSso"$tmp/awscli.zip" "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
        unzip -qqd "$tmp" "$tmp/awscli.zip"
        sudo "$tmp/aws/install"
    fi

    export PATH=~/.google-cloud-sdk/bin:$PATH
    if ! has_cmd gcloud; then
        log "installing gcloud"
        install_docs=https://cloud.google.com/sdk/docs/install
        download_re='https://[^"]+linux-x86_64.tar.gz'
        download_url=$(curl -fLSs "$install_docs" | grep -Eo "$download_re" | head -n1)
        curl -fLSs "$download_url" | tar xzC ~
        mv ~/google-cloud-sdk ~/.google-cloud-sdk
        ~/.google-cloud-sdk/install.sh \
            --path-update false \
            --bash-completion false \
            --usage-reporting false \
            --quiet >/dev/null
    fi

    log "installing python ${python_version:=$(vget python/cpython)}"
    asdf plugin list | grep -q python || asdf plugin add python
    asdf install python "$python_version"
    asdf global python "$python_version"

    log "installing pip packages"
    pip install -U "devtools[pygments]" pip pipenv poetry poetry-plugin-export setuptools wheel

    apply add_pip_pkg "${pip_pkgs[@]}"
    sitepackages=$(python -c 'import site; print(site.USER_SITE)')
    mkdir -p "$sitepackages"
    python -m devtools print-code >"$sitepackages/sitecustomize.py"
    # TODO colorize tracebacks via sitecustomize

    log "installing nodejs ${nodejs_version:=$(vget nodejs/node)}"
    asdf plugin list | grep -q nodejs || asdf plugin add nodejs
    asdf install nodejs "$nodejs_version"
    asdf global nodejs "$nodejs_version"

    log "installing npm packages"
    npm config set fund false
    npm install -g npm
    apply add_npm_pkg "${npm_pkgs[@]}"

    log "installing dotfiles"
    clone=~/.config/dotfiles
    rm -rf "$clone"
    git clone --bare https://gitlab.com/ambruss/dotfiles.git "$clone"
    dotfiles() { git --git-dir="$clone" --work-tree="$HOME" "$@"; }
    dotfiles remote set-url origin git@gitlab.home:ambruss/dotfiles.git
    dotfiles config --local status.showUntrackedFiles no
    dotfiles checkout -f
    chmod 700 ~/.ssh
    chmod 600 ~/.ssh/config
    test -f ~/.ssh/known_hosts || ssh-keyscan -H git{hub,lab}.com >~/.ssh/known_hosts 2>/dev/null
    rm -rf ~/{install.sh,LICENSE,README.md}

    log "symlinking shortcuts"
    symlink() { ln -sf "$(command -v "$2")" "$local_bin/$1"; }
    symlink d  docker
    symlink dc docker-compose
    symlink g  git
    symlink jq gojq
    symlink k  kubecolor

    log "updating tldr docs"
    test -d ~/.cache/tealdeer || tldr --update

    log "configuring docker"
    lnofile="[Service]\nLimitNOFILE=1048576\n"
    for svc in containerd docker; do
        svc_dir=/etc/systemd/system/$svc.service.d
        svc_conf=$svc_dir/override.conf
        test -d "$svc_dir"  || sudo mkdir -p "$svc_dir"
        test -f "$svc_conf" || printf "$lnofile" | sudo tee "$svc_conf" >/dev/null
    done
    docker build -h | grep -q buildx || docker buildx install
    add_group docker
    svc_enable docker

    log "configuring podman"
    sudo sed -Ei /etc/containers/registries.conf \
        -e's|^# (unqualified-search-registries).*|\1 = ["docker.io"]|'
    # https://wiki.archlinux.org/title/Podman
    # https://man.archlinux.org/man/binfmt.d.5.en
    # https://man.archlinux.org/man/systemd-binfmt.service.8.en
    # https://gist.githubusercontent.com/yuanfang-chen/d3ad4959727bbab8682e/raw/42e7093b1ff957715a2ab47afb3f68a93c7a27f2/qemu-binfmt-conf.sh
    # svc_enable systemd-binfmt

    log "configuring vscode"
    apply add_vscode_ext "${vscode_ext[@]}"

    log "configuring credentials"
    test ! -f ~/.config/zsh/env.zsh || . ~/.config/zsh/env.zsh
    apply zshenv \
        GIT_{NAME,EMAIL} \
        GIT{HUB,LAB}_TOKEN \
        DOCKER_{USER,PASS} \
        AWS_{ACCESS_KEY_ID,SECRET_ACCESS_KEY} \
        JIRA_{ENDPOINT,PROJECT,API_TOKEN}
    if [[ ! -f ~/.config/git/user && -n "$GIT_NAME" && -n "$GIT_EMAIL" ]]; then
        envtpl ~/.config/git/user.tpl
    fi
    if [[ ! -f ~/.docker/config.json && -n "$DOCKER_USER" && -n "$DOCKER_PASS" ]]; then
        docker login -u"$DOCKER_USER" --password-stdin <<<"$DOCKER_PASS"
    fi
    if [[ ! -f ~/.aws/config && -n "$AWS_ACCESS_KEY_ID" && -n "$AWS_SECRET_ACCESS_KEY" ]]; then
        aws configure set aws_access_key_id "$AWS_ACCESS_KEY_ID"
        aws configure set aws_secret_access_key "$AWS_SECRET_ACCESS_KEY"
        # TODO region?
    fi
    if [[ ! -f ~/.config/gcloud/configurations/config_default ]]; then
        gcloud init --skip-diagnostics
        gcloud auth application-default login
    fi

    # TODO update z4h (consider reinstall)
    log "bootstrapping z4h"
    grep -Eq "^$USER:.*/zsh$" /etc/passwd || sudo chsh -s /bin/zsh "$USER"
    has_cmd z4h || Z4H_BOOTSTRAPPING=1 . ~/.zshenv
}

# helpers
cred=$'\033[1;31m'; cgreen=$'\033[1;32m'; c0=$'\033[0m'
local_bin=~/.local/bin
local_comp=~/.local/zsh-completion
log() {( set +x; printf "${cgreen}[info]${c0} %b\n" "$*" >&2; )}
err() {( set +x; printf  "${cred}[error]${c0} %b\n" "$*" >&2; )}
die() { err "$@"; trap - ERR; exit 1; }
upper() { tr '[:lower:]' '[:upper:]'; }
lower() { tr '[:upper:]' '[:lower:]'; }
retry() { for _ in {1..3}; do "$@" && break || sleep 1; done; }
apply() { for arg in "${@:2}"; do "${1:?missing arg: command}" "$arg"; done; }
cache() { file="$tmp/$(echo "$*" | md5sum | head -c8)"; cat "$file" 2>/dev/null || "$@" | tee "$file"; }
ls_yay_pkgs() { yay -Qqe | sort; }
ls_pip_pkgs() { pip freeze | sed -E 's/==.*//;s/_/-/g' | lower | sort; }
ls_npm_pkgs() { npm ls -gp | tail -n+2 | sed -E 's/.*node_modules.//' | sort; }
has_yay_pkg() { cache ls_yay_pkgs | grep -wq "${1:?missing arg: yay package}"; }
has_pip_pkg() { cache ls_pip_pkgs | grep -wq "$(echo "${1:?missing arg: pip package}" | sed 's/\[.*\]//')"; }
has_npm_pkg() { cache ls_npm_pkgs | grep -wq "${1:?missing arg: npm package}"; }
add_yay_pkg() { has_yay_pkg "${1:?missing arg: yay package}" || yay -S --needed --noconfirm "$1"; }
add_pip_pkg() { has_pip_pkg "${1:?missing arg: pip package}" || pip install -U "$1"; }
add_npm_pkg() { has_npm_pkg "${1:?missing arg: npm package}" || npm install -g "$1"; }
add_gui_pkg() { ! has_gui || add_yay_pkg "${1:?missing arg: yay package}"; }
add_eget_bin() { has_cmd "${1/*\/}" || { eget "$@"; add_comp "${1/*\/}"; }; }
add_vscode_ext() { ! has_cmd code || code --install-extension "$1" &>/dev/null; }
has_cmd() { command -v "${1:?missing arg: command}" &>/dev/null; }
has_gui() { test -d /usr/share/xsessions; }
svc_enable() { systemctl is-active "${1:?missing arg: service}" &>/dev/null || sudo systemctl enable "$1.service"; }
add_group() { grep -Eq "^${1:?missing arg: group}:.*:$USER" /etc/group || sudo usermod -aG "$1" "$USER"; }
envtpl() { cat "${1:?missing arg: template}" | envsubst >"${1#.tpl}"; }
vpick() { vsort "$@" | head -n1; }
vsort() { sed '/-/!{s/$/_/}' "$@" | sort -ruV | sed 's/_$//'; }
vle() { test "$(printf '%s\n' "${1:?missing arg: v1}" "${2:?missing arg: v2}" | vpick)" = "$2"; }
vlt() { vle "${1:?missing arg: v1}" "${2:?missing arg: v2}" && test "$1" != "$2"; }
vgt() { ! vle "${1:?missing arg: v1}" "${2:?missing arg: v2}"; }
vge() { ! vlt "${1:?missing arg: v1}" "${2:?missing arg: v2}"; }
vget() { curl -fLSs "https://api.github.com/repos/$1/git/refs/tags" \
    | yq '.[].ref' | sed -En 's|refs/tags/v?([0-9.]+)$|\1|p' | vpick
}
add_comp() {
    cmd=${1:?missing arg: command}
    arg=$("$cmd" --help 2>&1 | grep -Ewo -- '^\s+-?-?(auto-?)?complet(e|ions?)' | head -n1 | sed -E 's/^\s+//' || :)
    test -z "$arg" || "$cmd" "$arg" zsh >"$local_comp/_$cmd"
}
zshenv() {
    envvar=${1:?missing arg: envvar}
    test -n "${!envvar:-}" || read -rp"Enter ${cgreen}$envvar${c0}: " "$envvar"
    test -n "${!envvar:-}" || return 0
    conf=~/.config/zsh/env.zsh
    line="export $envvar=\"${!envvar}\""
    if grep -Eq   "^export $envvar=.*" "$conf"; then
        sed -Ei "s|^export $envvar=.*|$line|" "$conf"
    else
        echo "$line" >>"$conf"
    fi
}

# packages
yay_pkgs=(
    age
    asdf-vm
    bat
    bat-extras
    bc
    btop
    cni-plugins
    cups-pdf
    dasel-bin
    direnv
    dive
    docker
    docker-buildx
    docker-compose
    docker-rootless-extras
    eget-bin
    expect
    fd
    fff
    fzf
    gdu-bin
    ghorg
    git
    git-delta
    git-extras
    git-lfs
    github-cli
    gitui
    glab
    go-jira-bin
    go-yq
    gojq
    # NOTE wrong checksum; unmaintained?
    # gomplate-bin
    hadolint-bin
    helm
    hexyl
    hq
    hyperfine
    jo
    julia
    k9s
    kubecolor
    kubectl
    libvirt
    lsd
    lua
    man
    man-pages
    micro
    moreutils
    nano-syntax-highlighting
    openssh
    openssl
    p7zip
    podman
    podman-compose
    podman-dnsname-git
    procs
    quemu-user-static
    quemu-user-static-binfmt
    r
    rancher-k3d
    ripgrep-all
    rust
    sd
    shellcheck-bin
    shfmt
    skopeo
    sops
    stern
    task
    tealdeer
    tmux
    tree
    unrar
    unzip
    wget
    xh
    xsv
    zig
    zip
    zoxide
    zsh
)

gui_pkgs=(
    displaylink
    evdi-git
    firefox
    flameshot
    google-chrome
    microsoft-edge-stable-bin
    onlyoffice-bin
    qbittorrent
    rustdesk-bin
    slack-desktop
    solaar
    spotify
    tk
    virt-manager
    visual-studio-code-bin
)

pip_pkgs=(
    addict
    asciinema
    black
    httpx"[cli]"
    humanfriendly
    ipython
    jinja2-cli
    pip-tools
    pre-commit
    ruff
    tqdm
    yamllint
)

npm_pkgs=(
    @prantlf/jsonlint
    markdownlint-cli
    prettier
    pyright
)

vscode_ext=(
    adrianwilczynski.alpine-js-intellisense
    christian-kohler.path-intellisense
    eamodio.gitlens
    GitLab.gitlab-workflow
    hangxingliu.vscode-systemd-support
    ms-azuretools.vscode-docker
    ms-kubernetes-tools.vscode-kubernetes-tools
    ms-python.python
    ms-python.vscode-pylance
    ms-vscode-remote.remote-containers
    otovo-oss.htmx-tags
    redhat.vscode-yaml
    tamasfe.even-better-toml
)

main "$@"
